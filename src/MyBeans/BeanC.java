package MyBeans;

public class BeanC  implements BeanValidator{
    private String name;
    private int value;

    public BeanC(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    private void init(){
        System.out.println("beanC is created");
    }

    private void destroy(){
        System.out.println("beanC is destroyed");
    }

    @Override
    public void validate() {
        if(name == null || value < 0){
            throw new IllegalArgumentException("Name or value is illegal");
        }
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
