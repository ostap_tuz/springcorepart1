package MyBeans;

public class BeanB implements BeanValidator {
    private String name;
    private int value;

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    private void init(){
        System.out.println("beanB is created");
    }

    private void destroy(){
        System.out.println("beanB is destroyed");
    }

    @Override
    public void validate() {
        if(name == null || value < 0){
            throw new IllegalArgumentException("Name or value is illegal");
        }
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
