package MyBeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("BeanBData.properties")
public class ConfigBeansInner {
    @Value("${beanB.name}")
    private String nameB;
    @Value("${beanB.value}")
    private int valueB;

    @Qualifier("beanB")
    @Bean(name = "beanB", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value = "beanD")
    public BeanB getBeanB()
    {
        BeanB beanB = new BeanB(nameB, valueB);
        beanB.validate();
        return beanB;
    }

    @Value("${beanC.name}")
    private String nameC;
    @Value("${beanC.value}")
    private int valueC;
    @Bean(name = "beanC", initMethod = "init", destroyMethod = "destroy")
    @DependsOn(value = "beanB")
    public BeanC getBeanC()
    {
        return new BeanC(nameC, valueC);
    }

    @Value("${beanD.name}")
    private String nameD;
    @Value("${beanD.value}")
    private int valueD;
    @Bean(name = "beanD", initMethod = "init", destroyMethod = "destroy")
    public BeanD getBeanD()
    {
        return new BeanD(nameD, valueD);
    }
}
