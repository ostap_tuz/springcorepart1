package MyBeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@Import(ConfigBeansInner.class)
@PropertySource("BeanBData.properties")
public class ConfigBeans {
    @Bean
    @Qualifier("beanA")
    @DependsOn(value = {"beanD","beanC","beanB"})
    public BeanValidator getBeanAFromBAndC(@Qualifier("beanB")BeanB beanB, @Qualifier("beanC")BeanC beanC)
    {
        return new BeanA(beanB, beanC);
    }

    @Bean
    @Qualifier("beanA")
    @DependsOn(value = {"beanD","beanC","beanB"})
    public BeanValidator getBeanAFromBAndD(@Qualifier("beanB")BeanB beanB, @Qualifier("beanD")BeanD beanD)
    {
        return new BeanA(beanB, beanD);
    }

    @Bean
    @Qualifier("beanA")
    @DependsOn(value = {"beanD","beanC","beanB"})
    public BeanValidator getBeanAFromCAndD(@Qualifier("beanC")BeanC beanC, @Qualifier("beanD")BeanD beanD)
    {
        return new BeanA(beanC, beanD);
    }

    @Bean
    public BeanValidator getBeanEFromBAndC(@Qualifier("getBeanAFromBAndC")BeanA beanA) { return new BeanE(beanA); }
    @Bean
    public BeanValidator getBeanEFromBAndD(@Qualifier("getBeanAFromBAndD")BeanA beanA) { return new BeanE(beanA); }
    @Bean
    public BeanValidator getBeanEFromCAndD(@Qualifier("getBeanAFromCAndD")BeanA beanA) { return new BeanE(beanA); }

    @Bean("beanF")
    @Lazy
    public BeanF getBeanF()
    {
        return new BeanF("Oleg", 34);
    }

    @Bean
    public BeanPost getBeanPost(){ return new BeanPost(); }

}
