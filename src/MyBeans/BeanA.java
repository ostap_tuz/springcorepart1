package MyBeans;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private String name;
    private int value;

    public BeanA(BeanB beanB, BeanC beanC){
        this.name = beanB.getName();
        this.value = beanC.getValue();
    }

    public BeanA(BeanB beanB, BeanD beanD){
        this.name = beanB.getName();
        this.value = beanD.getValue();
    }

    public BeanA(BeanC beanC, BeanD beanD){
        this.name = beanC.getName();
        this.value = beanD.getValue();
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public void validate() {
        if(name == null || value < 0){
            throw new IllegalArgumentException("Name or value is illegal");
        }
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("BeanA is not created");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("BeanA is created");
    }
}
