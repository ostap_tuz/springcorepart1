package MyBeans;

public class BeanF implements BeanValidator {
    private String name;
    private int value;

    public BeanF(String name, int value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public void validate() {
        if(name == null || value < 0){
            throw new IllegalArgumentException("Name or value is illegal");
        }
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
