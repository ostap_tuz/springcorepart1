package MyBeans;

public class BeanE implements BeanValidator {
    private String name;
    private int value;

    public BeanE(BeanA beanA) {
        this.name = beanA.getName();
        this.value = beanA.getValue();
    }

    @Override
    public void validate() {
        if(name == null || value < 0){
            throw new IllegalArgumentException("Name or value is illegal");
        }
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
