import MyBeans.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConfigBeans.class);

        applicationContext.getBean(BeanB.class);
        applicationContext.getBean(BeanC.class);
        applicationContext.getBean(BeanD.class);
        System.out.println("------------------");
        System.out.println(applicationContext.getBean("getBeanAFromBAndC"));
        System.out.println(applicationContext.getBean("getBeanAFromBAndD"));
        System.out.println(applicationContext.getBean("getBeanAFromCAndD"));
        System.out.println("------------------");
        System.out.println(applicationContext.getBean("getBeanEFromBAndC"));
        System.out.println(applicationContext.getBean("getBeanEFromBAndD"));
        System.out.println(applicationContext.getBean("getBeanEFromCAndD"));
        System.out.println("------------------");
        System.out.println(applicationContext.getBean(BeanF.class));
        applicationContext.getBean(BeanPost.class);

    }
}
